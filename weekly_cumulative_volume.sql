-- 1. Сколько авокадо типа organic было продано в Нью-Йорке и Лос Анжжелесе к концу каждой недели (накопительная сумма продаж)с разбивкой по годам?

SELECT region, date, total_volume, year,
    SUM(total_volume) OVER w AS cum_volume
FROM avocado
WHERE region in ('NewYork', 'LosAngeles') and type = 'organic'
group by region, date, total_volume, year

WINDOW w AS(
            PARTITION BY region, year
            ORDER BY date ASC
            ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
            )
Order by region DESC, date ASC

