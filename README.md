# Window functions SQL 
Несколько запросов, в которых есть оконные функции. 

## Stack
Запросы делала и строила графики в Redash.   
СУБД - ![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-FFF?style=for-the-badge&logo=PostgreSQL)

## Dataset
Данные по продажам авокадо находятся в датасете [avocado.csv](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/avocado.csv?ref_type=heads).  

название | тип | значение |
----------|-----|----------|
date | DATE | дата |
average_price | NUMERIC | средняя цена одного авокадо |
total_volume | NUMERIC | количество проданных авокадо |
plu4046 | NUMERIC | количество проданных авокадо PLU* 4046 |
plu4225 | NUMERIC | количество проданных авокадо PLU 4225 |
plu4770 | NUMERIC | количество проданных авокадо PLU 4770 |
total_bags | NUMERIC | всего упаковок |
small_bags | NUMERIC | маленькие упаковки |
large_bags | NUMERIC | большие упаковки |
xlarge_bags | NUMERIC | очень большие упаковки |
type | TEXT | тип авокадо: обычный или органический |
year | SMALLINT | год |
region | TEXT | город или регион (TotalUS – все регионы США) |

В таблице находятся данные на конец каждой недели. Для каждой даты есть несколько наблюдений, отличающихся по типу авокадо (organic & conventional) и региону продажи.   
*PLU — код товара (Product Lookup code)

## Реализация и результат
Каждый запрос отвечает на конкретный вопрос. Коды разместила в отдельные .sql файлы в репозитории и продублировала здесь. 

### 1. Кумулятивный объём 
Код [weekly_cumulative_volume](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/weekly_cumulative_volume.sql?ref_type=heads)
Для начала посмотрим на продажи авокадо в двух городах (NewYork, LosAngeles) и узнаем, сколько авокадо типа organic было продано в целом к концу каждой недели (накопительная сумма продаж) с разбивкой по году.
Значения внутри окна отсортируем по дате, а саму таблицу - по регионам (сначала NY, потом LA) и по возрастанию даты.

```sql
SELECT region, date, total_volume, year,
       SUM(total_volume) OVER w AS cum_volume
FROM avocado
WHERE region in ('NewYork', 'LosAngeles') and type = 'organic'
group by region, date, total_volume, year

WINDOW w AS(
            PARTITION BY region, year
            ORDER BY date ASC
            ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
            )
Order by region DESC, date ASC
```

![Таблица. Еженедельные продажи нарастающим итогом](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/tab_cum_vol.png?ref_type=heads)

![График. Продажи 2х регионов нарастающим итогом](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/weekly_cum_vol.png?ref_type=heads)

На графике видно, что в течение всего периода продажи в Лос Анжелесе опережали таковые в Нью-Йорке за исключением февраля 2018г. 

### 2. Сравнение с предыдущим периодом (неделей).
Код [Week-to-week comparison](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/w-to-w_comparison.sql?ref_type=heads)  
Когда объемы продаж обычных (conventional) авокадо резко падали по сравнению с предыдущей неделей?   
Возьмём данные по США в целом. 

```sql 
SELECT region, date, total_volume,
       total_volume - LAG(total_volume) OVER (ORDER BY date ASC) AS week_diff
FROM avocado
WHERE type = 'conventional' and region = 'TotalUS'
Order by date ASC
```

![Таблица. Сравнение объёмов продаж по сравнению с предыдущей неделей](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/tab_w-to-w.png?ref_type=heads)

![График. Сравнение недели к неделе](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/w-to-w_comparison.png?ref_type=heads)

На графике видно, что самые большие падения объемов продаж относительно предыдущей недели были в феврале каждого года. Самое крупное падение объемов продаж по сравнению с предыдущей неделей произошло 12 февраля 2017г.

### 3. Скользящее среднее. 
Код [rolling_price](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/rolling_price.sql?ref_type=heads)  
Посчитаем скользящее среднее цены авокадо в Нью-Йорке с разбивкой по типу авокадо.   
Окно - 3 недели: текущая и 2 предыдущие. Помним, что в строках содержатся данные за неделю, а не за один день.

```sql 
SELECT date, type,
       average_price, 
       AVG(average_price) OVER w AS rolling_price,
       region   
FROM avocado
WHERE region = 'NewYork'
WINDOW w AS(
            PARTITION by type
            Order by date
            ROWS BETWEEN 2 PRECEDING AND CURRENT ROW
            )
Order by type ASC, date ASC   
```

![Таблица. Cкользящая средняя цена авокадо в Нью-Йорке. Окно = 3 недели](https://gitlab.com/sql-projects3/window-functions-sql/-/blob/main/rolling_price.png?ref_type=heads)

## My contacts
Если у вас появились вопросы или предложения по сотрудничеству, пожалуйста, напишите мне

* в Телеграм @Natalie_l_Swan  
* или на почту tasha.l.swan@gmail.com
