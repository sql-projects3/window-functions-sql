-- 2. week-to-week comparison
-- Когда объемы продаж обычных (conventional) авокадо резко падали по сравнению с предыдущей неделей? 
-- Возьмём данные по США в целом.

SELECT region, date, total_volume,
       total_volume - LAG(total_volume) OVER (ORDER BY date ASC) AS week_diff
FROM avocado
WHERE type = 'conventional' and region = 'TotalUS'
Order by date ASC  
