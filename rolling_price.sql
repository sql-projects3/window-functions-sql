-- 3. rolling_price
-- Посчитаем скользящее среднее цены авокадо в Нью-Йорке с разбивкой по типу авокадо. 
-- Окно: текущая неделя и 2 предыдущие. Помним, что в строках содержатся данные за неделю, а не за один день.

SELECT date, type,
    average_price, 
    AVG(average_price) OVER w AS rolling_price,
    region   
FROM avocado
WHERE region = 'NewYork'
WINDOW w AS(
            PARTITION by type
            Order by date
            ROWS BETWEEN 2 PRECEDING AND CURRENT ROW
           )
Order by type ASC, date ASC  
